#############################################
# File: Main.py
# Python: Python3
# Author: Compute team
# Description: File containing startingpoint.
#              Calls the respective hook functions
#              based on the type of hook passed as an arg
##############################################
import subprocess
import sys
from stop_hook_executor import pre_stop_hook_executor

if __name__ == "__main__":
    hook = sys.argv[1]
    if hook == "pre_stop":
        subprocess.Popen("echo '++++++++++++++++++++++++++++' >> /proc/1/fd/1", shell=True, stdout=subprocess.PIPE).communicate()
        subprocess.Popen("echo  'CALLING PRE_STOP_HOOK_EXECUTOR ' >> /proc/1/fd/1", shell=True, stdout=subprocess.PIPE).communicate()
        subprocess.Popen("echo '++++++++++++++++++++++++++++' >> /proc/1/fd/1", shell=True, stdout=subprocess.PIPE).communicate()
        pre_stop_hook_executor()
    else:
        subprocess.Popen("echo '++++++++++++++++++++++++++++' >> /proc/1/fd/1", shell=True, stdout=subprocess.PIPE).communicate()
        subprocess.Popen("echo  'CURRENT HOOK TYPE IS NOT SUPPORTED' >> /proc/1/fd/1", shell=True, stdout=subprocess.PIPE).communicate()
        subprocess.Popen("echo '++++++++++++++++++++++++++++' >> /proc/1/fd/1", shell=True, stdout=subprocess.PIPE).communicate()
