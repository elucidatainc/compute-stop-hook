"""
Script containing function for Updating the
usage time of a notebook in the database.
"""
# Importing the libraries
import helper as hlpr
import json
import os
import subprocess


def get_usage_time():
    """
    Function for getting the usage time of the notebook
    It traces the process that initiated the notebook and
    fetches the start time for it.

    Return
    ------
    usage_time {int}: No of seconds the process has been running
    """
    usage_time = subprocess.Popen("ps -p 1 -o etimes=", shell=True, stdout=subprocess.PIPE).communicate()[0]
    return int(usage_time.decode('utf-8').strip())


# Main function for pre_stop_hook
def pre_stop_hook_executor():
    """
    Function for executing the main code on pre stop hook
    Function outputs the response to the docker stdout logs
    """
    # Fetching the POLLY_TYPE
    polly_type = hlpr.get_polly_env()
    api_url = "https://v2.api." + polly_type + "/projects/" + os.getenv('POLLY_PROJECT_ID') + "/jupyters/" + os.getenv('POLLY_NB_ID')
    api_payload = {
        "data": {
            "type": "jupyternotebook",
            "id": os.getenv('POLLY_NB_ID'),
            "attributes": {
                "usage_time": get_usage_time(),
                "machine": os.getenv('MACHINE')
            }
        }
    }
    api_payload = json.dumps(api_payload)
    api_headers = {
        'Content-Type': 'application/vnd.api+json'
    }

    cookies = {}
    if os.getenv('POLLY_API_KEY'):
        api_headers["x-api-key"] = os.getenv('POLLY_API_KEY')
    else:
        cookies = {
            os.getenv('POLLY_ID_TOKEN_KEY'): os.getenv('POLLY_ID_TOKEN'),
            os.getenv('POLLY_REFRESH_TOKEN_KEY'): os.getenv('POLLY_REFRESH_TOKEN')
        }

    resp = hlpr.make_api_call('PATCH', api_url, cookies, api_headers, api_payload)
    # Redirecting the output to the docker stdout logs
    subprocess.Popen("echo '++++++++++++++++++++++++++++' >> /proc/1/fd/1", shell=True, stdout=subprocess.PIPE).communicate()
    subprocess.Popen("echo 'PRE STOP HOOK EXECUTED' >> /proc/1/fd/1", shell=True, stdout=subprocess.PIPE).communicate()
    subprocess.Popen("echo 'RESPONSE STATUS: " + str(resp['status']) + " ' >> /proc/1/fd/1", shell=True, stdout=subprocess.PIPE).communicate()
    subprocess.Popen("echo 'RESPONSE MESSAGE: " + str(resp['message']) + " ' >> /proc/1/fd/1", shell=True, stdout=subprocess.PIPE).communicate()
    subprocess.Popen("echo '++++++++++++++++++++++++++++' >> /proc/1/fd/1", shell=True, stdout=subprocess.PIPE).communicate()
