# Script containing the helper functions
# that are used by the other parts of the code.
# Contain commonly used functions
import os
import requests


def get_polly_env():
    """
    Function for fetching the polly domain
    from the environment variable.
    POLLY_TYPE will vary according to the env like dev/test/prod

    Returns
    -------
    polly_env: string
        Eg: `devpolly.elucidata.io`
    """
    return os.getenv('POLLY_TYPE').split('://')[1]


def make_api_call(http_verb, endpoint, cookies, headers, payload=None):
    """
    Function for hitting the api request using the
    requests library.

    Parameters
    ----------
    http_verb {string} : HTTP METHOD type like GET, POST, PATCH etc
    endpoint {string}: API endpoint to make the request on
    cookies {dict}: Dict object conaining cookies in form of key value pair
    headers {dict}: Request headers to be passed during the request
    payload {dict}: (default None) Body to be passed to the endpoint while making requests

    Returns
    -------
    response: Dict
        object containing the response status and message (if)
    """
    response = requests.request(http_verb,
                                endpoint, data=payload,
                                headers=headers, cookies=cookies)
    final_response = {
        'status': response.status_code,
        'message': response.text
    }
    return final_response
